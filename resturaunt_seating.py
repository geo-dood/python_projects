# Author: George Maysack-Schlueter
user_in = input("How many people are in your dinner group? ")
table_seats = int(user_in)

if table_seats > 8:
    print(f"You will need to wait for a table of {table_seats} seats.")
else:
    print(f"Your table of {table_seats} seats is ready.")
