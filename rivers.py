# Author: George Maysack-Schlueter
list_num = 0

river_dict = {
    "nile"    :
        "africa",
    "missouri":
        "north america",
    "amazon"  :
        "south america",
    "yangtze" :
        "asia",
    "volga"   :
        "europe",
    "murray"  :
        "australia",
    "onyx"    :
        "antarctica",
}

for river, continent in river_dict.items():
    print(f"\nThe {river.title()} River is the longest river in "
          f"{continent.title()}!")

print()

for river in sorted(river_dict.keys()):
    list_num = list_num + 1
    print(f"{list_num}. The {river.title()} River")

list_num = 0
print()

for continent in sorted(river_dict.values()):
    list_num = list_num + 1
    print(f"{list_num}. {continent.title()}")
