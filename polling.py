# Author: George Maysack-Schlueter
favorite_food = {
    'alice'  : 'python',
    'bob'    : '',
    'charles': 'c',
    'daniel' : 'rust',
    'edward' : 'python',
    'frank'  : 'c',
    'george' : 'python',
    'hank'   : '',
    'imran'  : '',
    'john'   : 'python',
    'karen'  : '',
}

for person, food in favorite_food.items():
    if food:
        print(f"{person.title()}'s favorite food: {food}")
    else:
        print(f"{person.title()} has not responded to the poll.")
