# Author: George Maysack-Schlueter
custom_glossary = {
    "absquatulate (verb)" :
        "to leave somewhere abruptly",
    "amok (noun)"         :
        "an episode of sudden mass assault against people or objects",
    "bailiwick (noun)"    :
        "a person's area of skill, knowledge, authority, or work",
    "bamboozle (verb)"    :
        "to deceive or get the better of (someone) by trickery",
    "cryptozoology (noun)":
        "the study of evidence tending to substantiate the existence of, or the"
        " search for, creatures whose reported existence is unproven",
    "diphthong (noun)"    :
        "a sound formed by the combination of two vowels in a single syllable, "
        "in which the sound begins as one vowel and moves toward another",
    "festooned (noun)"    :
        "a string or chain of flowers, foliage, ribbon, etc., suspended in a "
        "curve between two points",
    "frangipani (noun)"   :
        "a perfume prepared from or imitating the odor of the flower of a "
        "tropical American tree or shrub",
    "fuddy-duddy (noun)"  :
        "a person who is stuffy, old-fashioned, and conservative",
    "interrobang (noun)"  :
        "a printed punctuation mark (‽), available only in some typefaces, "
        "designed to combine the question mark (?) and the exclamation point "
        "(!)"
}

for word, definition in sorted(custom_glossary.items()):
    print(f"\n{word.title()} - {definition.capitalize()}")
