# Author: George D. Maysack Schlueter
user0 = {"first"   : "alice", "last": "admin",
         "username": "a.admin0", "country": "usa"}
# noinspection SpellCheckingInspection
user1 = {"first"   : "bobby", "last": "brandit",
         "username": "b.brandit1", "country": "canada"}
# noinspection SpellCheckingInspection
user2 = {"first"   : "chuck", "last": "chezzen",
         "username": "c.chezzen2", "country": "france"}
user3 = {"first"   : "danny", "last": "dunko",
         "username": "d.dunko3", "country": "germany"}
user4 = {"first"   : "edward", "last": "eppen",
         "username": "e.eppen4", "country": "usa"}
user5 = {"first"   : "frank", "last": "frawno",
         "username": "f.frawno5", "country": "usa"}
user6 = {"first"   : "greg", "last": "glegor",
         "username": "g.glegor6", "country": ""}
user7 = {"first"   : "hank", "last": "habro",
         "username": "h.habro7", "country": "usa"}

people = [user0, user1, user2, user3, user4, user5, user6, user7]

for person in people:
    print(f"\nIdentity: {person['first'].title()} {person['last'].title()}")
    print(f"Username: {person['username']} ")

    if person['country'] == "":
        print("Location: Unknown")
    elif person['country'].lower() == "usa":
        print(f"Location: {person['country'].upper()}")
    else:
        print(f"Location: {person['country'].title()}")
