# Author: George Maysack-Schlueter
user_in = input("Please provide a number --> ")
user_num = int(user_in)

if user_num % 10 == 0:
    print(f"The number {user_num} is a multiple of 10!")
else:
    print(f"The number {user_num} is not a multiple of 10.")
